﻿

using VideoCalling.Views;

namespace VideoCalling
{
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();

            Routing.RegisterRoute(nameof(FirstPage), typeof(FirstPage));
            Routing.RegisterRoute(nameof(SecondPage), typeof(SecondPage));
        }

      
    }
}
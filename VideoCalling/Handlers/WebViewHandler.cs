﻿#if ANDROID
using Android.Graphics;
using Android.Webkit;
using Android.Widget;
using Android.App;
using static Android.Webkit.WebChromeClient;
using Android.Views;
using VideoCalling.Platforms.Android;
using Microsoft.Maui.Handlers;
#endif

namespace VideoCalling.Handlers
{
    public static class WebViewHandler
    {
        public static void CustomizeWebViewHandler()
        { 
#if ANDROID
            Microsoft.Maui.Handlers.WebViewHandler.Mapper.ModifyMapping(
                nameof(Android.Webkit.WebView.WebChromeClient),
                (handler, view, args) => handler.PlatformView.SetWebChromeClient(new MyWebChromeClient(handler)));
#endif
        }
    }
}

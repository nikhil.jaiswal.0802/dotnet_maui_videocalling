﻿using Microsoft.Extensions.Logging;
using Microsoft.Maui.Handlers;


namespace VideoCalling
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

            VideoCalling.Handlers.WebViewHandler.CustomizeWebViewHandler();

#if DEBUG
            builder.Logging.AddDebug();
#endif
            builder.Services.AddHybridWebView();
            return builder.Build();
        }
    }



}
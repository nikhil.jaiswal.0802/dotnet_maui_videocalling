package crc647e12b14a2740ca9f;


public class HybridWebView_HybridWebViewJavaScriptInterface
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_SendMessage:(Ljava/lang/String;)V:__export__\n" +
			"";
		mono.android.Runtime.register ("HybridWebView.HybridWebView+HybridWebViewJavaScriptInterface, HybridWebView", HybridWebView_HybridWebViewJavaScriptInterface.class, __md_methods);
	}


	public HybridWebView_HybridWebViewJavaScriptInterface ()
	{
		super ();
		if (getClass () == HybridWebView_HybridWebViewJavaScriptInterface.class) {
			mono.android.TypeManager.Activate ("HybridWebView.HybridWebView+HybridWebViewJavaScriptInterface, HybridWebView", "", this, new java.lang.Object[] {  });
		}
	}

	@android.webkit.JavascriptInterface

	public void sendMessage (java.lang.String p0)
	{
		n_SendMessage (p0);
	}

	private native void n_SendMessage (java.lang.String p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

﻿using Android.App;
using Android.Runtime;
using Microsoft.Maui.Handlers;
using VideoCalling.Platforms.Android;

namespace VideoCalling
{
    [Application]
    public class MainApplication : MauiApplication
    {
        public MainApplication(IntPtr handle, JniHandleOwnership ownership)
            : base(handle, ownership)
        {

        }



        protected override MauiApp CreateMauiApp() => MauiProgram.CreateMauiApp();
    }


}
﻿using Microsoft.Maui.Platform;
using Microsoft.Maui.Handlers;
using Android.Webkit;
using Microsoft.Maui.Controls.Compatibility.Platform.Android;
using Microsoft.Maui.Controls.Compatibility;
using VideoCalling;

namespace VideoCalling.Platforms.Android
{
    public class MyWebChromeClient : MauiWebChromeClient
    {    
        public MyWebChromeClient(IWebViewHandler handler) : base(handler)
        {
            //((IWebViewHandler)new MyWebView().Handler).PlatformView.SetWebChromeClient(new MyWebChromeClient((IWebViewHandler)new MyWebView().Handler));
        }
        //public override void OnPermissionRequest(PermissionRequest request)
        //{
        //    // Process each request
        //    foreach (var resource in request.GetResources())
        //    {
        //        // Check if the web page is requesting permission to the camera
        //        if (resource.Equals(PermissionRequest.ResourceVideoCapture, StringComparison.OrdinalIgnoreCase))
        //        {
        //            // Get the status of the .NET MAUI app's access to the camera
        //            PermissionStatus status = Permissions.CheckStatusAsync<Permissions.Camera>().Result;

        //            PermissionStatus status1 = Permissions.CheckStatusAsync<Permissions.Microphone>().Result;
        //            if (status != PermissionStatus.Granted && status1 != PermissionStatus.Granted)
        //                request.Deny();
        //            else
        //                request.Grant(request.GetResources());

        //            return;
        //        }
        //    }

        //    base.OnPermissionRequest(request);
        //}
        //
        public override void OnPermissionRequest(PermissionRequest request)
        {
            // Process each request
            bool cameraPermissionRequested = false;
            bool microphonePermissionRequested = false;

            foreach (var resource in request.GetResources())
            {
                if (resource.Equals(PermissionRequest.ResourceVideoCapture, StringComparison.OrdinalIgnoreCase))
                {
                    // Check if the web page is requesting permission to the camera
                    cameraPermissionRequested = true;
                }
                else if (resource.Equals(PermissionRequest.ResourceAudioCapture, StringComparison.OrdinalIgnoreCase))
                {
                    // Check if the web page is requesting permission to the microphone
                    microphonePermissionRequested = true;
                }
            }

            // Check camera and microphone permissions
            PermissionStatus cameraStatus = Permissions.CheckStatusAsync<Permissions.Camera>().Result;
            PermissionStatus microphoneStatus = Permissions.CheckStatusAsync<Permissions.Microphone>().Result;

            if (cameraPermissionRequested && microphonePermissionRequested)
            {
                // If both camera and microphone permissions are requested
                if (cameraStatus == PermissionStatus.Granted && microphoneStatus == PermissionStatus.Granted)
                {
                    request.Grant(request.GetResources());
                }
                else
                {
                    request.Deny();
                }
            }
            else if (cameraPermissionRequested)
            {
                // If only camera permission is requested
                if (cameraStatus == PermissionStatus.Granted)
                {
                    request.Grant(request.GetResources());
                }
                else
                {
                    request.Deny();
                }
            }
            else if (microphonePermissionRequested)
            {
                // If only microphone permission is requested
                if (microphoneStatus == PermissionStatus.Granted)
                {
                    request.Grant(request.GetResources());
                }
                else
                {
                    request.Deny();
                }
            }
            else
            {
                // Handle other types of permissions
                base.OnPermissionRequest(request);
            }
        }

    }
}

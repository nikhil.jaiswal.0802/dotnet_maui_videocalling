namespace VideoCalling.Views;

public class FirstPage : ContentPage
{
    WebView WebView;
    Button btnJoin;
    private static FirstPage instance;

    string html = @"<html><head></head><body><a href=""root/html/index.html"">Join</a></body></html>";

    public static FirstPage Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new FirstPage();
            }
            return instance;
        }
    }
    public FirstPage()
	{
        Title = "Video Calling";
        btnJoin = new Button
        {
            Text = "Join",
            WidthRequest = 100,
            HeightRequest = 40,
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center,
            Margin = 6,
        };
        btnJoin.Clicked += BtnJoin_Clicked;


       WebView = new WebView()
        {
           Source = new HtmlWebViewSource
           {
               Html = html
           }
        };

        Content = new VerticalStackLayout
		{
			Children = {
				btnJoin,
                //WebView
            }
		};
	}

    private async void BtnJoin_Clicked(object sender, EventArgs e)
    {
       var isAllowed = await RequestPermission();

        if (isAllowed) { await Navigation.PushAsync(new SecondPage()); } 
    }

    private async Task<bool> RequestPermission()
    {
        var status = await Permissions.CheckStatusAsync<Permissions.Camera>();
        if (status != PermissionStatus.Granted)
        {
            var response = await Permissions.RequestAsync<Permissions.Camera>();
            if (response != PermissionStatus.Granted)
            {
                return false;
            }
        }

        var statusMic = await Permissions.CheckStatusAsync<Permissions.Microphone>();
        if (statusMic != PermissionStatus.Granted)
        {
            var response = await Permissions.RequestAsync<Permissions.Microphone>();
            if (response != PermissionStatus.Granted)
            {
                return false;
            }
        }

        return true;       
    }
}
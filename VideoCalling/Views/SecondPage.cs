
using DevExpress.Entity.Model.Metadata;
using DevExpress.XtraRichEdit.Model;
using HybridWebView;
using Microsoft.JSInterop;
using Microsoft.Maui.Controls.PlatformConfiguration;
using Microsoft.Maui.Handlers;
using Plugin.Permissions;
using System.Drawing;



namespace VideoCalling.Views;

public class SecondPage : ContentPage
{
    WebView myWebView;
    ImageButton btnMic;
    ImageButton btnLeave;
    ImageButton btnCamera;
    HybridWebView.HybridWebView hybridView;

    public SecondPage()
	{       

        Title = "Video Calling";

        string html = @"<html><body><script> window.onload = async () => { window.location.href = ""root/index.html""}</script></body></html>";

        myWebView = new WebView()
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            Source = new HtmlWebViewSource
            {
                Html = html
            },
        };
        myWebView.SetValue(Grid.RowSpanProperty, 2);
        myWebView.SetValue(Grid.RowProperty, 0);

        hybridView = new HybridWebView.HybridWebView()
        {
            VerticalOptions = LayoutOptions.Fill,
            HorizontalOptions = LayoutOptions.Fill,
            HybridAssetRoot = "root/html",
            MainFile = "index.html"
        };
        hybridView.SetValue(Grid.RowSpanProperty, 2);
        

        btnLeave = new ImageButton()
        {
            Margin = new Thickness(10),
            HeightRequest = 30,
            WidthRequest = 60,
            HorizontalOptions = LayoutOptions.Center,
            BackgroundColor = Colors.Transparent,
            Source = "call_end_100.png"
        };
        btnLeave.Clicked += BtnLeave_Clicked;

        btnCamera = new ImageButton()
        {
            Margin = new Thickness(10),
            HeightRequest = 30,
            WidthRequest = 60,
            HorizontalOptions = LayoutOptions.Center,
            BackgroundColor = Colors.Transparent,
            Source = "camera_off_100.png"
        };
        btnCamera.Clicked += BtnCamera_ClickedAsync;

        btnMic = new ImageButton()
        {
            Margin = new Thickness(10),
            //CornerRadius = 50,
            HeightRequest = 30,
            WidthRequest = 60,
            HorizontalOptions = LayoutOptions.Center,
            BackgroundColor = Colors.Transparent,
            Source = "mic_off_100.png"
        };
        btnMic.Clicked += BtnMic_ClickedAsync;

        StackLayout buttonLayout = new StackLayout
        {
            Margin = new Thickness(0, 0, 10, 0),
            BackgroundColor = Colors.Transparent,
            Orientation = StackOrientation.Horizontal,
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.End,
            Children =
            {           
                 new Frame()
                {
                    HasShadow = true,
                    CornerRadius = 50,
                    Margin = new Thickness(10),
                    HeightRequest = 60,
                    WidthRequest = 60,
                   // Padding = 1,
                    BorderColor = Colors.LightGray,
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                     BackgroundColor= Colors.Gray,
                    Content = btnMic,
                },
                new Frame()
                {
                    HasShadow = true,
                    CornerRadius = 50,
                    Margin = new Thickness(10),
                    HeightRequest = 60,
                    WidthRequest = 60,
                   // Padding = 1,
                    BorderColor = Colors.LightGray,
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Center,
                    BackgroundColor= Colors.Red,
                    Content = btnLeave
                },
                
                  new Frame()
                {
                    HasShadow = true,
                    CornerRadius = 50,
                    HeightRequest = 60,
                    Margin = new Thickness(10),
                    WidthRequest = 60,
                   // Padding = 1,
                    BorderColor = Colors.LightGray,
                    HorizontalOptions = LayoutOptions.End,
                    VerticalOptions = LayoutOptions.Center,
                    BackgroundColor= Colors.Gray,
                    Content = btnCamera
                }
            }
        };

        buttonLayout.SetValue(Grid.RowProperty,2);


       
        Content = new Grid
        {
            RowDefinitions =
            {
                new RowDefinition { Height = new GridLength(9) },
                new RowDefinition { Height = new GridLength(1,GridUnitType.Star) }
            },
            Children =
            {
#if ANDROID
                hybridView,
#elif IOS
                myWebView,
#endif
                buttonLayout
            }
        };

    }

    private async void BtnMic_ClickedAsync(object sender, EventArgs e)
    {
        await hybridView.EvaluateJavaScriptAsync("toggleMic();");
    }

    private async void BtnCamera_ClickedAsync(object sender, EventArgs e)
    {
        await hybridView.EvaluateJavaScriptAsync("Camera();");
    }

    protected override async void OnAppearing()
    {
        base.OnAppearing();
        //await Task.Delay(1000);
       // await hybridView.EvaluateJavaScriptAsync("joinCall()");
    }

    private async void BtnLeave_Clicked(object sender, EventArgs e)
    {
        await hybridView.EvaluateJavaScriptAsync("leaveAndRemoveLocalStream();");
        await Navigation.PopAsync();
    }

    private async void Hybrid_RawMessageReceived(object sender, HybridWebViewRawMessageReceivedEventArgs e)
    {
        await Dispatcher.DispatchAsync(async () =>
        {
            await DisplayAlert("JavaScript message", e.Message,"Ok");
        });
    }

    protected async override void OnDisappearing()
    {
        base.OnDisappearing();
        await hybridView.EvaluateJavaScriptAsync("leaveAndRemoveLocalStream();");
    }

}